import sqlite3
import random
import time
from  pyrogram import *
from datetime import datetime, timedelta

try:
    file = open('base.db')
except IOError as e:
    f = open('base.db', 'w')
    f.close()
    conn = sqlite3.connect('base.db')
    c = conn.cursor()
    c.execute ('''CREATE TABLE dict (name text, code text)''')
    c.execute ('''CREATE TABLE saved (name text, code text, qty integer)''')
    conn.commit()
    conn.close()
else:
    with file:
        print(u'делаем что-то с файлом')


bot_name = 'ChatWarsBot'
helper_bot = 'CWCastleBot'
team_chat = 'ЧыРвань n.e. Скала'
capitan_bot = 'DarkDesireNewOrderBot'
app = Client("chwars1")
stop = False
g_def = False
g_atk = False
g_masters = ['treshka', 'greenglaz', 'shitglaz', 'Rudeak']
war = [8, 16, 0]
atacker = True
spin = True
defer = False
raider = False
bomzh = False
gold_limit = 0
sliv_timer = 50
sliv_item = '01'
slito_gold = False
in_sliv = False


orders = {
    'red': '🇮🇲',
    'black': '🇬🇵',
    'white': '🇨🇾',
    'yellow': '🇻🇦',
    'blue': '🇪🇺',
    'mint': '🇲🇴',
    'twilight': '🇰🇮',
    'lesnoi_fort': '🌲Лесной форт',
    'les': '🌲Лес',
    'liho': "\ud83c\udf42\u041b\u0438\u0445\u043e\u043b\u0435\u0441\u044c\u0435",
    'topi': "\ud83e\udddf\u200d\u2640\ufe0f\u041c\u0435\u0440\u0442\u0432\u044b\u0435 \u0422\u043e\u043f\u0438",
    'loshchina': "\ud83c\udf0b\u041b\u043e\u0449\u0438\u043d\u0430 \u0414\u044c\u044f\u0432\u043e\u043b\u0430",
    'cover': '🛡Защита',
    'attack': '⚔Атака',
    'cover_symbol': '🛡',
    'hero': '\ud83c\udfc5\u0413\u0435\u0440\u043e\u0439',
    'corovan': '/go',
    'dolina': '\u26f0\ufe0f\u0414\u043e\u043b\u0438\u043d\u0430',
    'quests': '🗺 Квесты',
    'castle_menu': '🏰Замок',
    'lavka': '🏚Лавка',
    'make': '⚒Изготовить',
    'shlem': 'Шлем',
    'res': '\ud83d\udce6\u0420\u0435\u0441\u0443\u0440\u0441\u044b',
    'lvl_def': '+1 🛡Защита',
    'lvl_atk': '+1 ⚔Атака',
    'lvl_off': 'Выключен',
    'boloto':'\ud83c\udf44\u0411\u043e\u043b\u043e\u0442\u043e',
    'pet_play': '⚽Поиграть',
    'pet_feed': '🍼Покормить',
    'pet_wash': '🛁Почистить',
    'quest': '\ud83d\uddfa\u041a\u0432\u0435\u0441\u0442\u044b'
}

"""
  [
     "\ud83d\udc22",
     "\u2618\ufe0f",
     "\ud83c\udf39"
 ],
 [
     "\ud83c\udf41",
     "\ud83d\udda4",
     "\ud83c\udf46"
 ]
 """  
castles = {
    u'\ufe0f\ud83d\udda4':'\ud83d\udda4', #scala
    u'\ud83d\udda4':'\ud83d\udda4', #scala
    u'\ufe0f\ud83e\udd87':'\ud83e\udd87', #night
    u'\ud83e\udd87':'\ud83e\udd87', #night
    u'night':'\ud83e\udd87', #night
    u'\ud83c\udf46':'\ud83c\udf46', #ferma
    u'\ud83c\udf41':'\ud83c\udf41', #amber
    u'\u2618\ufe0f':'\u2618\ufe0f', #oplot
    u'\ud83d\udc22':'\ud83d\udc22' #tortuga
    }
money = 0
sleep = False
defer = False
time_to_update = datetime.now()
random.seed()
 
def message_parser(chat_id, text):
    global sleep
    global time_to_update
    global raider
    global in_sliv
    if text.find ('восстановлена: ') > -1 or text.find ('some energy +1') >-1 :
        tempapp = Client("chwars2")
        tempapp.start()
        q = random.randint (0,2)
        if q == 0:
            tempapp.send_message (bot_name, orders['les'])
        if q == 1:
            tempapp.send_message (bot_name, orders['boloto'])
        if q == 2:
            tempapp.send_message (bot_name, orders['dolina'])
        tempapp.stop()
    
    if text.find ('/go')>0 and text.find('bind') == -1 and text.find('КОРОВАН')>0:
        time.sleep (random.randint(10, 179)) 
        app.send_message (bot_name, '/go')
    if text.find ('/pledge')>0:
        time.sleep (random.randint(10,118))
        app.send_message (bot_name, '/pledge')
    if text.find ('/engage')>0:
        time.sleep (random.randint(1,4))
        app.send_message (bot_name, '/engage')
        time.sleep (10)
    if text.find ('Склад') >-1 and raider:
        sklad_sliv(text)
    if text.find ('Купить 1:') >-1 and in_sliv:
        trader(text)
    i = 0
    sleep = False
    for line in text.splitlines():
        if line.find('Отдых') > -1:
            sleep = True
            print ('Режим отдыха')
    for line in text.splitlines():
       # print ('Cтрока #'+str(i) + ' '+line)
        i = i+1

        if line.find (u'Битва семи замков')>-1:
            print ('Минут до начала битвы:' + str(time_to_attack(line)))

        if line.find (u'💰') >-1:
            print ('Строка денег')
            get_money (line)

        if line.find ('🔋Выносливость:')>-1:
            if int (stam_count (line))>0 and sleep:
                tempapp = Client("chwars2")
                print ('хватит отдыхать')
                tempapp.start()
                q = random.randint (0,2)
                if q == 0:
                    tempapp.send_message (bot_name, orders['quest'])
                    time.sleep (random.randint(1, 3))
                    tempapp.send_message (bot_name, orders['les'])
                if q == 1:
                    tempapp.send_message (bot_name, orders['quest'])
                    time.sleep (random.randint(1, 3))
                    tempapp.send_message (bot_name, orders['boloto'])
                if q == 2:
                    tempapp.send_message (bot_name, orders['quest'])
                    time.sleep (random.randint(1, 3))
                    tempapp.send_message (bot_name, orders['dolina'])
                tempapp.stop()
                sleep = False
        
        
def time_to_attack(line):
    time_a = 0
    if line.find ('мин.') >0 or line.find ('минуты!') >0 :
        line = line [:line.rfind(' ')].strip()
        minutes = line [line.rfind(' '):len(line)].strip()
        time_a = time_a+int(minutes)
    if line.find ('ч.') >0:
        line = line [:line.rfind('ч.')].strip()
        hours = line [line.rfind(' '):len(line)].strip()
        time_a= time_a+int (hours)*60
    return time_a 

def stam_count (line):
    global time_to_update
    print (line)
    if line.find ('мин.') == -1:
        time_to_update = 0
        return 1
    stam = line [:line.rfind('/')]
    stam = stam [stam.rfind(' '):len(stam)].strip()
    time = line [line.rfind('⏰')+1:len(line)]
    time = time [:time.rfind('мин.')]
    timeInt = int(time) + random.randint(0,6)
    time_to_update = datetime.now() + timedelta (minutes = timeInt )
    print ('time to update '+str(time_to_update))
    print ('Стамины есть'+ stam)
    return stam

def  get_money (line):
    global money
    print (line)
    if line.find(' ') == -1:
        money = int (line[1:len(line)])
    else:
        try:
            money = int (line[1:line.find(' ')].strip())
        except:
            print ('Позиция пробела в мешочках' + str (line.find(' ')))
    print (str(money//2))
    return money

def sklad_sliv (text):
    for line in text.split(')'):
        res = line.split ('(')
        SQL = "SELECT * FROM dict WHERE name ='"+res[0].strip()+"'"
        conn = sqlite3.connect('base.db') 
        c = conn.cursor()
        c.execute(SQL)
        answer = ''
        for row in c:
            answer = 'Найден ресурс '+res[0].strip()+' с кодом ' + row[1]+' в количестве '+res[1].strip()+'\n'
            if answer != '':
                app.send_message(me, answer)
            c1 = conn.cursor()
            SQL = "SELECT * FROM saved WHERE code = '"+ row[1] +"'"
            c1.execute (SQL)
            qty_to_guild = int(res[1].strip())
            for raw1 in c1:
                qty_to_guild = qty_to_guild - int(str(raw1[2]).strip())
            if qty_to_guild > 0:
                deposit =''
                deposit = '/g_deposit '+str(row[1].strip())+' '+str(qty_to_guild)
                tempapp = Client("chwars2")
                tempapp.start()
                tempapp.send_message (bot_name, deposit.strip())
                tempapp.stop()
        conn.commit()
        conn.close()
    time.sleep (random.randint(1, 5))
        
"""
  [
     "\ud83d\udc22",
     "\u2618\ufe0f",
     "\ud83c\udf39"
 ],
 [
     "\ud83c\udf41",
     "\ud83d\udda4",
     "\ud83c\udf46"
 ]
 """ 



def zamok_parse (key):
    key = key.replace (u'\ufe0f', '')
    key = key.strip()
    #app.send_message (me, key)
    print (key)
    if key == '\ufe0f\ud83d\udda4':
        app.send_message (me, "skala1" )
        return '\ud83d\udda4' #scala
    if key ==  '\ud83d\udda4':
        app.send_message (me, "skala2" )
        return '\ud83d\udda4' #scala
    if key == u"\U0001F5A4":
        app.send_message (me, "skala3" )
        return '\ud83d\udda4' #scala 
    if key ==u"\U0001F987":
        app.send_message (me, "night3" )
        return '\ud83e\udd87' #scala 
    if key == '\ufe0f\ud83e\udd87':
        app.send_message (me, "night1" )
        return '\ud83e\udd87' #night
    if key == '\ud83e\udd87': #night
        app.send_message (me, "night2" )
        return '\ud83e\udd87'
    if key == '\ud83c\udf46':
        app.send_message (me, "ferma1" )
        return '\ud83c\udf46' #ferma
    if key == u"\U0001F346":
        app.send_message (me, "ferma2" )
        return '\ud83c\udf46' #ferma    
    if key == u"\U0001F341":
        app.send_message (me, "amber1" )
        return '\ud83c\udf41' 
    if key == '\ud83c\udf41': #amber
        app.send_message (me, "amber2" )
        return '\ud83c\udf41'
    if key == '\u2618\ufe0f': #oplot
        app.send_message (me, "oplot1" )
        return '\u2618\ufe0f'
    if key == '\ufe0f\u2618\ufe0f':#oplot
        app.send_message (me, "oplot2" )
        return '\u2618\ufe0f'
    if key == '\u2618':#oplot
        app.send_message (me, "oplot3" )
        return '\u2618\ufe0f'
    if key == u"\u2618":#oplot
        app.send_message (me, "oplot4" )
    if key == '\ud83d\udc22': #tortuga
        app.send_message (me, "tortuga1" )
        return '\ud83d\udc22'
    if key == u"\U0001F422": #tortuga
        app.send_message (me, "tortuga2" )
        return '\ud83d\udc22'
    if key =='\ud83c\udf39':
        app.send_message (me, "roza1" )
        return '\ud83c\udf39' #roza
    if key == u"\U0001F339":
        app.send_message (me, "roza2" )
        return '\ud83c\udf39' #roza
    return orders['cover']  




def razvorot_parser (text):
    print('Информация о развороте')
    global defer
    inattack = False 
    #print (text)
    #if text.find(orders['attack'])>-1:
    for line in text.splitlines():
        print (line)
        if line.find ('Сводки с полей:') > 0:
            return 1
        if not defer:
            print ('not defer')
            print (line)
            if line.find('⚔')>-1 and line.find(castles['night']) == -1: #
                    print ('atack line')
                    
                    tempapp = Client("chwars2")
                    tempapp.start()
                    tempapp.send_message (bot_name, orders['attack'])
                    tempapp.stop()
                    tempapp = Client("chwars2")
                    print ('атакуем '+  line.replace('⚔️','').strip())
                    time.sleep (random.randint(1, 3))
                    tempapp.start()
                    tempapp.send_message (bot_name, zamok_parse(line.replace('⚔','').strip()))
                    inattack = True
                    tempapp.stop()
                    return 1
            if line.find('⚔️')>-1 and line.find(castles['night']) > -1:
                    tempapp = Client("chwars2")
                    tempapp.start()
                    tempapp.send_message (bot_name, orders['cover'])
                    print ('Дэфаемся')
                    inattack = False
                    tempapp.stop()
                    return 1
        else:
            if line.find(orders['cover']) > -1 and line.find('🦇') > -1 and line.find(':') == -1 and line.find('#') == -1:
                        tempapp = Client("chwars2")
                        tempapp.start()
                        tempapp.send_message (bot_name, orders['cover'])
                        print ('Дэфаемся')
                        inattack = False
                        tempapp.stop()
                        return 1

            



def gold_sliv():
    global money
    global sliv_item
    global slito_gold
    global in_sliv
    tempapp = Client("chwars2")
    tempapp.start()
    tempapp.send_message (bot_name,'/t_'+sliv_item)
    in_sliv = True
    
    tempapp.stop()

def trader (text):
    app.send_message(me, text)
    global slito_gold
    global money
    global sliv_item
    
    cheapest = text.split('\n')[1]
    cheapest = cheapest [cheapest.find('по')+3:len(cheapest)-1].strip()
    answer = '/wtb_'+sliv_item+'_'+str((money-gold_limit)//int(cheapest))
    tempapp = Client("chwars2")
    tempapp.start()
    tempapp.send_message (bot_name, answer)
    slito_gold = True
    tempapp.stop()



def options (message):
        global money
        global sleep
        global time_to_update
        global me
        global stop
        global g_atk
        global g_def
        global atacker
        global spin
        global defer
        global raider
        global bomzh
        global slito_gold
        global sliv_item
        global sliv_timer
        global gold_limit
        

        if message.text == "/stop":
            if stop:
                stop = False
                g_def = False
                app.send_message (me, "Бот запущен")
            else:
                stop = True
                g_def = False
                app.send_message (me, "Бот остановлен")
        if message.text == "/start":
                stop = False
                g_def = False
                app.send_message (me, "Бот запущен")
        if message.text == "/clear_sliv":
            app.send_message (me, "Слив прочищен! Можно опять тратися перед битвой")
            slito_gold = False
            return
        if message.text =="/atack": 
            if atacker:
                app.send_message (me, "Ждем комманды от руководства на защиту гильдии")
                g_def = True
                atacker = False
                return
            else:
                atacker = True
                g_def = False
                app.send_message (me, "Мочить врагов!!!!")
        if message.text =="/spin": 
            if spin:
                app.send_message (me, "Заебали ваши карусели - сижу дома")
                g_def = False
                spin = False
            else:
                app.send_message (me, "Крутится-вертится шар голубой! Жду разворота")
                g_def = True
                spin = True
        if message.text =="/def": 
            if defer:
                app.send_message (me, "Дома скучно - буду ходить в атаку")
                atacker = True
                g_def = False
                defer = False
            else:
                app.send_message (me, "Защищаем дом родной!!!")
                atacker = False
                g_def = False
                defer = True
        if message.text =="/raider":
            if raider:
                app.send_message (me, "Моя прелесссьть! Не отдам никому свой сток")
                raider = False
            else:
                app.send_message (me, "Хозяин! Нас хотят ограбить. Ты точно спрятал нитки и камни?")
                raider = True
        if message.text =="/bomzh":
            if bomzh:
                app.send_message (me, "А зачем сливать голду если на нас не идут?")
                bomzh = False
            else:
                app.send_message (me, "Я бычок подниму, горький дым затяну... Бутылочки не найдется?")
                bomzh = True
        if message.text.find("/timer") ==0:
            sliv_timer = int(message.text.split(' ')[1])
            app.send_message (me, "Время собирать камни будет в "+str(sliv_timer)+" минут")
        if message.text.find("/wtbs") ==0:
            sliv_item = message.text.split('_')[1]
            app.send_message (me, "Покупаем ресурс "+sliv_item)
        if message.text.find("/limit") ==0:
            gold_limit = int(message.text.split(' ')[1])
            app.send_message (me, "Золотишка оставим "+str(gold_limit))


        if message.text =="/help":
             app.send_message (me, """**Список доступных комманд:**
**/stop** - остановить/запустить бота
**/start** - запустить бота
**/atack** - игнорировать команды защиты гильдии
**/status** - статус бота
**/spin** - включить/выключить слежение за разворотами
**/def** - защищать замок при разворотах
**/raider** - слить сток в гильдию
**/add_{item name}_{code}** - добавить код ресурса в справочник
**/list** - список известных ресурсов
**/save {code} {qty}** - сохранять {qty} количество ресурсов в стоке
**/del {code}** - удалить ресурс из охраняемых
**/saved** - список охраняемых ресурсов
**/del_all** - очистить все охраняемые ресурсы
**/bomzh** - сливать золото перед битвой
**/timer {minutes}** - в сколько минут перед битвой слить золото
**/wtbs_{code}** - на что сливаем голду
**/limit {qty}** - сколько голды оставить
**/clear_sliv** - очистить признак слива голды (бот забудет что уже сливал)
**/destroy_{code}_{qty}** - уничтожить на верстаке qty ресурсов по коду code""")
        if message.text.find('/add') > -1:
            cList = message.text.split('_')
            if len (cList) > 3:
                cList[1]=cList[1]+' ' +cList[2]
                cList[2] = cList[3]
            SQL = "INSERT INTO dict VALUES ('"+cList[1]+"','"+cList[2]+"')"
            conn = sqlite3.connect('base.db') 
            c = conn.cursor()
            try:
                c.execute(SQL)
                conn.commit()
                answer = ''
                answer = 'Ресурс ' + cList[1] + ' с кодом '+cList[2]+ ' добавлен'
                app.send_message (me, answer)
            except:
                answer = 'Ошибка добавления ресурса'
                app.send_message (me, answer)
            conn.close()
        if message.text.find('/list') > -1:
            cList = message.text.split()
            SQL = "SELECT * FROM dict"
            conn = sqlite3.connect('base.db') 
            c = conn.cursor()
            c.execute(SQL)
            answer ='Известные коды ресурсов:\n'
            for row in c:
                answer = answer+row[0]+' '+row[1]+'\n'
            conn.commit()
            conn.close()
            app.send_message (me, answer)
        if message.text.find('/saved') > -1:
            SQL = "SELECT * FROM saved"
            conn = sqlite3.connect('base.db') 
            c = conn.cursor()
            c.execute(SQL)
            answer ='**Список охраняемых ресурсов:**\n'
            for row in c:
                answer = answer+row[0]+' '+row[1]+' ('+str(row[2]) +')\n'
            conn.commit()
            conn.close()
            app.send_message (me, answer)
            return 1
        if message.text.find ('/del_all') == 0:
            conn = sqlite3.connect('base.db') 
            c = conn.cursor()
            SQL = "DELETE FROM saved"
            c.execute(SQL)
            conn.commit()
            conn.close()
            app.send_message (me,"Таблица охраняемых ресурсов очищена")
            return 1
        if message.text.find ('/destroy') == 0:
            cList = message.text.split('_')
            qty = int(cList[2].strip()) // 50
            tempapp = Client("chwars2")
            tempapp.start()
            tempapp.send_message (me,'Уничтожаем '+ str(qty) + ' раз')
            tempapp.stop()
            i = 0
            while i <= qty:
                answer = '/a_'+str(cList[1])+' '+'50'
                tempapp = Client("chwars2")
                tempapp.start()
                tempapp.send_message (bot_name, answer)
                tempapp.stop() 
                time.sleep (random.randint(4, 10))
                tempapp = Client("chwars2")
                tempapp.start()
                tempapp.send_message (bot_name,'⚒Изготовить')
                tempapp.send_message (me,'Уничтожено '+ str(i*50))
                tempapp.stop()
                i = i+1
        if message.text.find('/save') > -1:
            cList = message.text.split()
            if len (cList) > 3:
                cList[1]=cList[1]+' ' +cList[2]
                cList[2] = cList[3]
            conn = sqlite3.connect('base.db') 
            c = conn.cursor()
            try:
                SQL = "SELECT * FROM dict WHERE code = '" + cList[1]+"'"
                c.execute(SQL)
                item =''
                for row in c:
                    item = row[0]
                app.send_message (me, 'item name' + item)
                SQL = "INSERT INTO saved VALUES ('"+item+"','"+cList[1]+"','"+cList[2]+"')"
                c.execute(SQL)
                conn.commit()
                answer = ''
                answer = 'Ресурс ' + item + ' с кодом '+cList[1]+ ' сохранен в количестве ' + cList[2]
                app.send_message (me, answer)
            except:
                answer = 'Ошибка сохранения ресурса'
                app.send_message (me, answer)
            conn.close()
        if message.text.find('/del') > -1:
            cList = message.text.split()
            conn = sqlite3.connect('base.db') 
            c = conn.cursor()
            try:
                SQL = "DELETE FROM saved WHERE code = '" + cList[1]+"'"
                c.execute(SQL)
                conn.commit()
                answer = ''
                answer = 'Ресурс с кодом '+cList[1]+ ' удален из охраняемых' + cList[2]
                app.send_message (me, answer)
            except:
                answer = 'Ошибка сохранения ресурса'
                app.send_message (me, answer)
            conn.close()
        if message.text == '/status':
            answer = '**Состояние бота:**\n'
            if g_def:
                answer = answer + "1. Бот в состоянии защиты гильдии\n"
            else:
                answer = answer + "1. Бот следит за разворотами\n"
            if atacker:
                answer = answer + "2. Бот будет игнорировать команды защиты гильдии и будет атаковать при развороте\n"
            else:
                if not defer:
                    answer = answer + "2. Бот следит за командами гильдмастера и игнорирует развороты\n"
                else:
                     answer = answer + "2. Бот следит за разворотами и в состоянии защиты замка\n"
            if defer:
                answer = answer + "3. Бот всегда будет защищать замок при развороте\n"
            else:
                answer = answer + "3. Бот **НЕ** будет защищать замок при развороте\n"
            if raider:
                answer = answer +  "4. Бот **будет** сливать сток в гильдию\n"
            else:
                answer = answer + "4. Бот **НЕ** будет сливать сток в гильдию\n"
            if bomzh:
                answer = answer + "5. Сливаем голду в " +str(sliv_timer) + "минут. Покупаем " +sliv_item +". Оставляем "+ str(gold_limit) +" золота"
            app.send_message (me, answer)
        
        


@app.on_message(Filters.text)
def echo(client, message):
    global money
    global sleep
    global time_to_update
    global me
    global stop
    global g_atk
    global g_def
    global atacker
    global spin
    global defer
    global raider
    global bomzh
    global slito_gold
    global in_sliv
    
    try:
        if message.chat.type != 'channel' and message.from_user.username == me:
            options (message)
    except Exception as ex:
        template = "Произошла ошибка {0}. **Параметры ошибки**:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        app.send_message (me, message)


    if not stop:
        if bomzh:
            if not slito_gold:
                if not in_sliv:
                    if datetime.now().hour in war:
                        if datetime.now().minute >= sliv_timer:
                            gold_sliv()
        #if datetime.now == time_to_update:
        #            tempapp = Client("chwars2")
        #            print ('обновляемся')
        #            tempapp.start()
        #            tempapp.send_message (bot_name, orders['hero']) #'\ud83c\udf32\u041b\u0435\u0441'
        #            tempapp.stop()
        
        try:
                if message.chat.type != 'channel':
                    print (message)
                    if message.from_user.username == bot_name:
                        message_parser (message.chat.id, message.text)
                    if message.from_user.username == helper_bot:
                        print (message.text)
                        if message.text.find ('/wtb_02_') >-1:
                            print (message.text)
                            count = str (money//2)
                            print (count)
                            app.send_message (bot_name, '/wtb_02_'+str(money//2))
                if message.chat.title == team_chat :
                    print (message.text)
                    if not g_def:
                        if message.from_user.username == capitan_bot and not g_def and spin:
                            if message.text.find ('/f_')>0:
                                app.send_message(bot_name, message.text)
                            razvorot_parser(message.text)
                            
                    
                    if message.from_user.username in g_masters:
                        if message.text.find ('/g_def') > -1 and not atacker:
                            #print (message.text)
                            guild_name = ''
                            if len(message.text[message.text.find ('/g_def')+6:].strip()) == 3:
                                guild_name = message.text[message.text.find ('/g_def')+6:message.text.find ('/g_def')+10].strip()
                            if len(message.text[message.text.find ('/g_def')+6:].strip()) == 2:
                                guild_name = message.text[message.text.find ('/g_def')+6:message.text.find ('/g_def')+9].strip()
                            if len (guild_name)>0:
                                guild_name = guild_name.upper()
                                answer = ''
                                answer = '/g_def '+guild_name
                            else:
                                answer = '/g_def'
                            app.send_message (me, 'master order received ' + answer)
                            app.send_message(bot_name, answer)
                            g_def = True
                    
        except Exception as ex:
                print ('____________________ERROR WITH MESSAGE___________________')
                print (message)
                print ('____________________END ERROR WITH MESSAGE___________________')
                template = "Произошла ошибка {0}. **Параметры ошибки**:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                app.send_message (me, message)
       
        
    

app.start()
print (app.get_me())
me = app.get_me().username
#app.send_message (me, "Бот запущен")
app.send_message (bot_name, orders['les'])
#tempapp = Client("chwars2")
#tempapp.start()
#tempapp.send_message (bot_name,'/g_def')
#tempapp.stop()

app.idle()


